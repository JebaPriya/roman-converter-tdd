package com.thoughtworks.vapasi;

public class Rectangle {


    private float length;
    private float breadth;

    public Rectangle(float length, float breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public float calculateArea() {
        return this.length * this.breadth;
    }

    public float calculatePerimeter() {
        return 2 * (this.length + this.breadth);
    }
}
