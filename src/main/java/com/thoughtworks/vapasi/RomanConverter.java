package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Map;

public class RomanConverter {

    Map<Character, Integer> romanBaseNumeralsMappedWithArabic;

    RomanConverter(){
        initializeRomanBaseNumeralsMappedWithArabic();
    }

    private void initializeRomanBaseNumeralsMappedWithArabic() {
        romanBaseNumeralsMappedWithArabic = new HashMap<Character, Integer>();

        romanBaseNumeralsMappedWithArabic.put('I', 1);
        romanBaseNumeralsMappedWithArabic.put('V', 5);
        romanBaseNumeralsMappedWithArabic.put('X', 10);
        romanBaseNumeralsMappedWithArabic.put('L', 50);
        romanBaseNumeralsMappedWithArabic.put('C', 100);
        romanBaseNumeralsMappedWithArabic.put('D', 500);
        romanBaseNumeralsMappedWithArabic.put('M', 1000);
    }

    public Integer convertRomanToArabicNumber(String roman) {
        Integer result;

        if(isRomanNull(roman) || isRomanEmpty(roman))
            result = -1;
        else if(isInvalidRoman(roman))
            throw new IllegalArgumentException("Invalid roman numeral.");
        else
            result = calculateArabicNumberFromRoman(roman);

        return result;
    }

    private boolean isRomanEmpty(String roman){
        return roman.length() == 0;
    }

    private boolean isRomanNull(String roman){
        return roman == null;
    }

    private boolean isInvalidRoman(String roman) {
        boolean isInvalidRoman = false;

        for(char romanCharacter : convertRomanNumeralToCharacterArray(roman)){
            if(!isRomanCharacterAvailableInRomanBaseNumeralsMappedWithArabic(romanCharacter)){
                isInvalidRoman = true;
                break;
            }
        }

        return isInvalidRoman;
    }

    private boolean isRomanCharacterAvailableInRomanBaseNumeralsMappedWithArabic(char romanCharacter){
        return romanBaseNumeralsMappedWithArabic.containsKey(romanCharacter);
    }

    private char[] convertRomanNumeralToCharacterArray(String roman){
        return roman.toCharArray();
    }

    private Integer calculateArabicNumberFromRoman(String roman){
        Integer arabicNumber = 0;

        Integer arabicValueOfPreviousRomanCharacter = 0;

        for(char romanCharacter : convertRomanNumeralToCharacterArray(roman)) {

            Integer arabicValueOfCurrentRomanCharacter = getArabicValueOfRomanCharacter(romanCharacter);

            if( isSmallerValuePrecedingLargerValue(arabicValueOfPreviousRomanCharacter, arabicValueOfCurrentRomanCharacter)) {
                arabicNumber -= arabicValueOfPreviousRomanCharacter;
                arabicNumber += arabicValueOfCurrentRomanCharacter - arabicValueOfPreviousRomanCharacter;
            } else {
                arabicNumber += getArabicValueOfRomanCharacter(romanCharacter);
            }

            arabicValueOfPreviousRomanCharacter = arabicValueOfCurrentRomanCharacter;
        }

        return arabicNumber;
    }

    private Integer getArabicValueOfRomanCharacter(char roman){
        return romanBaseNumeralsMappedWithArabic.get(roman);
    }

    private boolean isSmallerValuePrecedingLargerValue(Integer precedingRomanCharacter, Integer succeedingRomanCharacter){
        return (precedingRomanCharacter < succeedingRomanCharacter);
    }

}
