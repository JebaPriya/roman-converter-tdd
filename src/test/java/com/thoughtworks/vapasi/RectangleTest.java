package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest{

    @Test
    void shouldCalculateAreaGivenValidInput(){
        float length = 5.0f;
        float breadth = 10.0f;
        float expectedArea = 50.0f;
        Rectangle rectangle = new Rectangle(length, breadth);
        float actualArea = rectangle.calculateArea();

        assertEquals( expectedArea, actualArea, "Wrong calculation");
    }

    @Test
    void shouldCalculatePerimeterGivenValidInput(){
        float length = 10.0f;
        float breadth = 20.0f;
        float expectedPerimeter = 60.0f;
        Rectangle rectangle = new Rectangle(length, breadth);
        float actualPerimeter = rectangle.calculatePerimeter();

        assertEquals(expectedPerimeter, actualPerimeter, "Wrong perimeter calculation");
    }
}
