package com.thoughtworks.vapasi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class RomanConverterTest {

    @ParameterizedTest
    @MethodSource("shouldProvideArgumentsForConvertRomanNumeral")
    void shouldConvertRomanNumerals(String romanNumeral, Integer arabic){
        RomanConverter romanConvertor = new RomanConverter();

        assertEquals(arabic, romanConvertor.convertRomanToArabicNumber(romanNumeral));
    }

    private static Stream<Arguments> shouldProvideArgumentsForConvertRomanNumeral() {
        return Stream.of(
                Arguments.of("I", 1),
                Arguments.of("II", 2),
                Arguments.of("III", 3),
                Arguments.of("IV", 4),
                Arguments.of("V", 5),
                Arguments.of("VI", 6),
                Arguments.of("VII", 7),
                Arguments.of("IX", 9),
                Arguments.of("X", 10),
                Arguments.of("XXXVI", 36),
                Arguments.of("MMXII", 2012),
                Arguments.of("MCMXCVI", 1996)
        );
    }

    @Test
    void shouldCheckEmptyInputForRomanNumeral() {
        Integer expected = -1;
        Integer actual;
        String romanNumeral = "";
        RomanConverter romanConvertor = new RomanConverter();

        actual = romanConvertor.convertRomanToArabicNumber(romanNumeral);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckNullInputForRomanNumeral() {
        Integer expected = -1;
        Integer actual;
        RomanConverter romanConvertor = new RomanConverter();

        actual = romanConvertor.convertRomanToArabicNumber(null);

        assertEquals(expected, actual);
    }

    @Test
    void shouldThrowIllegalArgumentExceptionWhenInvalidRomanValueIsPassed() {
        String romanNumeral = "FG";
        RomanConverter romanConvertor = new RomanConverter();

        assertThrows(IllegalArgumentException.class,
                () -> {
                       romanConvertor.convertRomanToArabicNumber(romanNumeral);
        });
    }
}